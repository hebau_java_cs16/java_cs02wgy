class Date {
		private int year;
		private int month;
		private int day;
		public Date () {
		}
		public Date(int year,int month,int day) {
			this.year=year;
			this.month=month;
			this.day=day;
		}
		public void geteyear(int year) {
			this.year=year;
		}
		public int setyear() {
			return year;
		}
		public void getetmonth(int month) {
			this.month=month;
		}
		public int setmonth() {
			return month;
		}
		public void getday(int day) {
			this.day=day;
		}
		public int setday() {
			return day;
		}
		public String toString() {
			return year+"-"+month+"-"+day;
		}
}
