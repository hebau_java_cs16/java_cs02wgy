class Player implements Comparable<Player>{
	private String number;
	private String name;
	private double score;
	public Player() {
		
	}
	public Player(String number,String name){
		this.number = number;
		this.name = name;
	}
	public void setname(String name) {
		this.name = name;  
	}
	public String getname() {
		return name;
	}
	public void setnumber(String number) {
		this.number = number;  
	}
	public String getnumber() {
		return number;
	}
	public void setscore(double score) {
		this.score=score;
	}
	public double getscore() {
		return score;
	}
	public String toString(){
		return "��ţ�"+this.number+"\t������"+this.name+"\t������"+this.score;
	}
	public int compareTo(Player o){ 
		if (this.score > o.score) { 
			return -1;   
        }
		else if (this.score < o.score) { 
			return 1; 
		}
		else { 
           return 0; 
		} 
	} 
}
