class Rater{
	private int number;
	double score[]= null;
	public Rater() {
		System.out.println("无参构造方法被调用了。") ;
	}
	public Rater(int number){
		this.number = number;
		score=new double [number];
	}
	public int getnumber( ){
		return number;
	}
	public void setnumber(int number) {
		this.number = number;
	}
	public void setscore(double score[] ) {
		for(int i=0;i<score.length ;i++) {
			this.score[i] = score[i];
		}
	}
	public double Avarage() {
		double min=score[0];
		double max=score[0];
		double sum=0;
		for(int i=0;i<this.number;i++) {
			sum+=score[i];
			if(score[i]>max) {
				max=score[i];
			}
			if(score[i]<min) {
				min=score[i];
			}
		}
		System.out.println("最高分是"+max);	
		System.out.println("最低分是"+min);
		return (double)(sum-max-min)/(number-2);
	}
}