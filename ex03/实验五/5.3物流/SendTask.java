
public class SendTask {
	private String number;
	private String weight;
	SendTask(){
		
	}
	SendTask(String number,String weight){
		this.number=number;
		this.weight=weight;
	}
	public void setnumber(String number) {
		this.number=number;
	}
	public String getnumber() {
		return number;
	}
	public void setweight(String weight) {
		this.weight=weight;
	}
	public String getweight() {
		return weight;
	}
	public void sendBefore(){
		System.out.println("订单开始处理，仓库开始验货...");
		System.out.println("货物重量:"+this.weight+"kg");
		System.out.println("订单已发货");
		System.out.println("快递单号:"+this.number);
		System.out.println();
	}
	public void send(Transportation t,GPS tool) {
		t.transport();
		tool.showCoordinate();
		System.out.println();
	}
	public void sendAfter(Transportation t) {
		System.out.println("运货人"+t.name+"所驾驶编号为"+t.num+"的"+t.model+"已归还！ ");     
	}
}
