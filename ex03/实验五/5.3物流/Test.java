import java.util.Scanner;

public class Test {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("请输入送货人姓名，运输编号，运输车型：");
		String name = input.next();
		String num = input.next();
		String model = input.next();
		JDTransportation JD=new JDTransportation(num,model,name);
		System.out.println("请输入GPS：");
		double Lng=input.nextDouble();
		double Lat=input.nextDouble();
		Phone p=new Phone(Lng,Lat);
		System.out.println("请输入货物重量，快递单号");
		String weight = input.next();
		String number = input.next();
		SendTask s =new SendTask(number,weight);
		s.sendBefore();
		s.send(JD, p);
		s.sendAfter(JD);
	}
}
