
public class JDTransportation extends Transportation{
	JDTransportation(String num,String model,String name){
		super(num,model,name);
	}
	public void transport() {
		System.out.println("运货人"+super.name+"正在驾驶编号"+super.num+"的"+super.model+"发送货物");
	}
}
