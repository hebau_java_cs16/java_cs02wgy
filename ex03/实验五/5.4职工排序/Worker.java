import java.util.Date; 
import java.text.SimpleDateFormat;
public class Worker implements Comparable<Worker>{
	private String number;
	private String name;
	private String sex;
	private Date birth;
	private Department departmentname;
	private Date worktime;
	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
	Worker(){
		
	}
	Worker(String number,String name,String sex){
		this.number = number;
		this.name = name;
		this.sex = sex;
	}
	public Date getBirth() {
		return birth;
	}
	public void setBirth(Date birth) {
		this.birth = birth;
	}
	public Date getWorktime() {
		return worktime;
	}
	public void setWorktime(Date worktime){
		this.worktime = worktime;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public Department getDepartmentname() {
		return departmentname;
	}
	public void setDepartmentname(Department departmentname) {
		this.departmentname = departmentname;
	}

	public String toString() {
		return "职工号："+number+"姓名:"+name+"性别:"+sex+"生日"+sdf.format(birth)+"参加工作时间:"+sdf.format(worktime);
	} 
	public int compareTo(Worker o) {
		String s1=sdf.format(this.getBirth()),s2=sdf.format(o.getBirth());
		if(s1.compareTo(s2)>0){
			return 1;
		}
		else if(s1.compareTo(s2)<0){
			return -1;
		}
		return 0;
	}
}
