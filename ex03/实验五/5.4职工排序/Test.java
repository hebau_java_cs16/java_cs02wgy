import java.util.Arrays;
import java.util.Date; 
import java.util.Scanner;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
class Test { 
  public static void main(String[] args) throws Exception{ 
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
    Scanner input = new Scanner(System.in);
    System.out.println("输入部门数");
    int n=input.nextInt();
    Department A[]=new Department [n];
    for (int i =0;i<A.length;i++) {
    	System.out.println("请输入部门编号：");
		String a=input.next();
		System.out.println("请输入部门姓名：");
		String b=input.next();
		System.out.println("请输入部门人数：");
		int m=input.nextInt();
		A[i]=new Department(a,b,m);
		Worker B[]=new Worker [m];
		System.out.println("默认第一个为经理");
		for(int j=0;j<B.length;j++) {
			System.out.println("请输入第"+(j+1)+"个职工的职工号");
			String c=input.next();
			System.out.println("请输入第"+(j+1)+"个职工的姓名");
			String d=input.next();
			System.out.println("请输入第"+(j+1)+"个职工的性别");
			String e=input.next();
			B[j]=new Worker(c,d,e);
			if(j==0) {
				A[i].setWorkname(d);
			}
			System.out.println("请输入第"+(j+1)+"个职工的生日");
			String f=input.next();
			B[j].setBirth(sdf.parse(f));
			System.out.println("请输入第"+(j+1)+"个职工的参加工作时间");
			String g=input.next();
			B[j].setWorktime(sdf.parse(g));
			System.out.println(B[j].toString());
		}
		A[i].setWork(B);
		System.out.println(A[i].toString());
    }
    for(int i=0;i<A.length;i++) {
    	A[i].toString();
    	Worker B[]=A[i].getWork();
    	Arrays.sort(B);
    	for (int j=0;j<A[i].getWork().length;j++) {
    		System.out.println(B[j].toString());
    	}
    }
  } 
}
class Cmp implements Comparator <Worker>{
	SimpleDateFormat str=new SimpleDateFormat("yyyy-MM-dd");
	public int compare(Worker o1, Worker o2) {
		String s1=str.format(o1.getBirth()),s2=str.format(o2.getBirth());
		if(s1.compareTo(s2)>0){
			return 1;
		}
		else if(s1.compareTo(s2)<0){
			return -1;
		}
		return 0;
	}

}
