
public class Department {
	private String number;
	private String name;
	private String workname;
	private Worker work[]=null;
	Department(){
		
	}
	Department(String number,String name,int n){
		this.number = number;
		this.name = name;
		Worker work[]=new Worker[n];
	}
	public Worker[] getWork() {
		return work;
	}
	public void setWork(Worker[] work) {
		this.work = work;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getWorkname() {
		return workname;
	}
	public void setWorkname(String workname) {
		this.workname = workname;
	}
	public String toString() {
		return "部门编号:"+number+"部门名称："+name+"\n 经理"+workname;
	}
}
