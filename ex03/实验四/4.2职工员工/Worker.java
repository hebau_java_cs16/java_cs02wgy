class Worker {
	private String name;
	private int age;
	private String sex;
	public Worker() {
		
	}
	public Worker(String name,int age,String sex) {
		this.name=name;
		this.age=age;
		this.sex=sex;
	}
	public void setname(String name) {
		this.name=name;
	}
	public String getname() {
		return name;
	}
	public void setage(int age) {
		this.age=age;
	}
	public int getage() {
		return age;
	}
	public void setsex(String sex) {
		this.sex=sex;
	}
	public String getsex() {
		return sex;
	}
	public String toString() {
		return "姓名："+name+"年龄"+age+"性别属性"+sex;
	}
}
