import java.util.Scanner;
public class Bank {
	static String bankName="建设银行";
	private String name;
	private String password;
	private double  balance;
	private double  turnover;
	Scanner input=new Scanner(System.in);
	public Bank() {
		
	}
	public Bank(String name,String password,double balance) {
		this.name=name;
		this.password=password;
		this.balance=balance-10;
		System.out.println("还剩金额"+this.balance);
	}
	public static void welcome( ) {
		System.out.println("欢迎您使用"+bankName+"银行系统");
	}
	public void setname(String name) {
		this.name=name;
	}
	public String getname() {
		return name;
	}
	public void setpassword(String password) {
		this.password=password;
	}
	public String getpassword() {
		return password;
	}
	public void setturnover(int turnover) {
		this.turnover=turnover;
	}
	public double getturnover() {
		return turnover;
	}
	public void setbalance(int balance) {
		this.balance=balance;
	}
	public double getbalance() {
		return balance;
	}
	public void deposit() {
		System.out.println("请输入要存的金额：");
		turnover=input.nextDouble();
		balance+=turnover;
		System.out.println("您存入"+turnover+"，还剩金额"+balance);
	}
	public void withdrawal() {
		System.out.println("请输入密码：");
		String password=input.next();
		if(password.equals(this.password)==false){
			System.out.println("密码错误");
		}
		else {
			System.out.println("账户余额"+balance+",请输入要取的金额：");
			turnover=input.nextDouble();
			if(turnover>balance) {
				System.out.println("您输入的金额过大，不能进行交易");
			}
			else {
				balance-=turnover;
				System.out.println("您取出"+turnover+"，还剩金额"+balance);
			}
		}
	}
	public static void welcomeNext( ) {
		System.out.print("请携带号随身财物，欢迎下次光临银行");
	}
}
