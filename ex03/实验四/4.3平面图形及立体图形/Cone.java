
public class Cone extends Pillar{//Բ׶
	double h;
	double l;
	double R;
	Geometry bottom;
	Cone(Geometry bottom,double h,double l,double R){
		this.bottom=bottom;
		this.h=h;
		this.l=l;
		this.R=R;
	}
	public double getVolume() {
		if(h==0) {
			return -1;
		}
		return bottom.getArea()*h/3.0;
	}
	public double Surface_area() {
		return (bottom.getArea()+R*3.14*l);//
	}
}
