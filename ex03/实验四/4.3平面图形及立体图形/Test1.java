import java.util.Scanner;
import java.util.Random;
/*public*/ class Test1 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("输入所要计算的图形");
		System.out.println("1.球");
		System.out.println("2.圆柱");
		System.out.println("3.圆锥");
		System.out.println("4.矩形");
		System.out.println("5.三角型类");
		System.out.println("6.圆类");
		System.out.println("请输入1——6：");
		int n=input.nextInt();
		switch(n){
		case 1:
			System.out.println("请输入的半径：");
			double R=input.nextDouble();
			Geometry A1 =new Circle (R);
			Pillar A2=new Ball(A1,R);
			System.out.printf("体积是%f",A2.getVolume());
			System.out.println("体积是"+A2.Surface_area());
			break;
		case 2:
			System.out.println("请输入圆柱的底部半径和高：");
			double R1=input.nextDouble();
			double H=input.nextDouble();
			Geometry B1 =new Circle (R1);
			Pillar B2=new Ball(B1,H);
			System.out.println("体积是"+B2.getVolume());
			System.out.println("体积是"+B2.Surface_area());
			break;
		case 3:
			System.out.println("请输入圆锥的底部半径和高以及母线：");
			double R2=input.nextDouble();
			double H1=input.nextDouble();
			double L=input.nextDouble();
			Geometry C1 =new Circle (R2);
			Pillar C2=new Cone(C1,H1,L,R2);
			System.out.println("体积是"+C2.getVolume());
			System.out.println("体积是"+C2.Surface_area());
			break;
		case 4:
			System.out.println("请输入矩形的长宽：");
			double l=input.nextDouble();
			double w=input.nextDouble();
			Geometry D =new Rectangle (l,w);
			System.out.println("面积是"+D.getArea());
			System.out.println("周长是"+D.getPerimeter());
			break;
		case 5:
			System.out.println("请输入三角形a，b，c三条边以及在a上的高：");
			double a=input.nextDouble();
			double b=input.nextDouble();
			double c=input.nextDouble();
			double h=input.nextDouble();
			Geometry E =new Triangle (a,b,c,h);
			System.out.println("面积是"+E.getArea());
			System.out.println("周长是"+E.getPerimeter());
			break;
		case 6:
			System.out.println("请输入圆的半径：");
			double r=input.nextDouble();
			Geometry F =new Circle (r);
			System.out.println("面积是"+F.getArea());
			System.out.println("周长是"+F.getPerimeter());
			break;
		}
	}

}
