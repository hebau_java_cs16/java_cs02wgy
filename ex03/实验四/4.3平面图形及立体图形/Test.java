import java.util.Scanner;
import java.util.Random;
import java.math.BigDecimal;
public class Test {
	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("输入所要计算的图形");
		System.out.println("1.球");
		System.out.println("2.圆柱");
		System.out.println("3.圆锥");
		System.out.println("4.矩形");
		System.out.println("5.三角型类");
		System.out.println("6.圆类");
		System.out.println("请输入1——6：");
		Random rand=new Random();
		int x=rand.nextInt(5)+1;
		int y=rand.nextInt(5)+1;
		int z=rand.nextInt(5)+1;
		int u=rand.nextInt(5)+1;
		int n=input.nextInt();
		BigDecimal m1,m2;
		double V1,S1,V,S;
		switch(n){
		case 1:
			double R=x;
			System.out.println("球的半径："+R);
			Geometry A1 =new Circle (R);
			Pillar A2=new Ball(A1,R);
			System.out.println("请输入图形的体积和表面积：");
			V=input.nextDouble();
			m1=new BigDecimal(A2.getVolume());  
			V1=m1.setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue();  
			if(V1==V) {
				System.out.println("图形的体积回答正确");
			}
			else {
				System.out.println("图形的体积回答错误");
			}
			System.out.println("体积是"+V1);
			S=input.nextDouble();
			m2=new BigDecimal(A2.Surface_area());  
			S1=m2.setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue();  
			if(S==S1) {
				System.out.println("图形的表面积回答正确");
			}
			else {
				System.out.println("图形的表面积回答错误");
			}
			System.out.println("表面积是"+S1);
			break;
		case 2:
			double R1=x;
			double H=y;
			System.out.println("圆柱的半径："+R1);
			System.out.println("圆柱的高："+H);
			Geometry B1 =new Circle (R1);
			Pillar B2=new Cylinder(B1,H);
			System.out.println("请输入图形的体积和表面积：");
			V=input.nextDouble();
			m1=new BigDecimal(B2.getVolume());  
			V1=m1.setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue();  
			if(V1==V) {
				System.out.println("图形的体积回答正确");
			}
			else {
				System.out.println("图形的体积回答错误");
			}
			System.out.println("体积是"+V1);
			S=input.nextDouble();
			m2=new BigDecimal(B2.Surface_area());  
			S1=m2.setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue();  
			if(S==S1) {
				System.out.println("图形的表面积回答正确");
			}
			else {
				System.out.println("图形的表面积回答错误");
			}
			System.out.println("表面积是"+S1);
			break;
		case 3:
			double R2=x;
			double H1=y;
			double L=z;
			System.out.println("圆锥的底部半径："+R2);
			System.out.println("圆锥的底部高："+H1);
			System.out.println("圆锥的底部母线："+L);
			Geometry C1 =new Circle (R2);
			Pillar C2=new Cone(C1,H1,L,R2);
			System.out.println("请输入图形的体积和表面积：");
			V=input.nextDouble();
			m1=new BigDecimal(C2.getVolume());  
			V1=m1.setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue();  
			if(V1==V) {
				System.out.println("图形的体积回答正确");
			}
			else {
				System.out.println("图形的体积回答错误");
			}
			System.out.println("体积是"+V1);
			S=input.nextDouble();
			m2=new BigDecimal(C2.Surface_area());  
			S1=m2.setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue();  
			if(S==S1) {
				System.out.println("图形的表面积回答正确");
			}
			else {
				System.out.println("图形的表面积回答错误");
			}
			System.out.println("表面积是"+S1);
			break;
		case 4:
			double l=x;
			double w=y;
			System.out.println("请输入矩形的长："+l);
			System.out.println("请输入矩形的宽："+w);
			Geometry D =new Rectangle (l,w);
			System.out.println("请输入图形的面积和周长：");
			V=input.nextDouble();
			m1=new BigDecimal(D.getArea());  
			V1=m1.setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue();  
			if(V1==V) {
				System.out.println("图形的面积回答正确");
			}
			else {
				System.out.println("图形的面积回答错误");
			}
			System.out.println("面积是"+V1);
			S=input.nextDouble();
			m2=new BigDecimal(D.getPerimeter());  
			S1=m2.setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue();  
			if(S==S1) {
				System.out.println("图形的表面积回答正确");
			}
			else {
				System.out.println("图形的表面积回答错误");
			}
			System.out.println("周长是"+S1);
			break;
		case 5:
			double a=u;
			double b=x;
			double c=y;
			double h=z;
			System.out.println("请输入三角形a边："+a);
			System.out.println("请输入三角形b边："+b);
			System.out.println("请输入三角形c边："+c);
			System.out.println("请输入三角形在a上的高："+h);
			Geometry E =new Triangle (a,b,c,h);
			System.out.println("请输入图形的面积和周长：");
			V=input.nextDouble();
			m1=new BigDecimal(E.getArea());  
			V1=m1.setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue();  
			if(V1==V) {
				System.out.println("图形的面积回答正确");
			}
			else {
				System.out.println("图形的面积回答错误");
			}
			System.out.println("面积是"+V1);
			S=input.nextDouble();
			m2=new BigDecimal(E.getPerimeter());  
			S1=m2.setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue();  
			if(S==S1) {
				System.out.println("图形的表面积回答正确");
			}
			else {
				System.out.println("图形的表面积回答错误");
			}
			System.out.println("周长是"+S1);
			break;
		case 6:
			double r=x;
			System.out.println("请输入圆的半径R："+r);
			Geometry F =new Circle (r);
			System.out.println("请输入图形的面积和周长：");
			V=input.nextDouble();
			m1=new BigDecimal(F.getArea());  
			V1=m1.setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue();  
			if(V1==V) {
				System.out.println("图形的面积回答正确");
			}
			else {
				System.out.println("图形的面积回答错误");
			}
			System.out.println("面积是"+V1);
			S=input.nextDouble();
			m2=new BigDecimal(F.getPerimeter());  
			S1=m2.setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue();  
			if(S==S1) {
				System.out.println("图形的表面积回答正确");
			}
			else {
				System.out.println("图形的表面积回答错误");
			}
			System.out.println("周长是"+S1);
			break;
		}
	}
}