public class Rectangle extends Geometry {//����
	double l;
	double w;
	Rectangle(double l,double w) {
        this.l=l;
        this.w=w;
    }
	public void setl(double l) {
	    	this.l=l;
	}
	public double getl() {
	   	return l;
	}
	public void setw(double w) {
	  	this.w=w;
	}
    public double getw() {
    	return w;
    }
    public double getArea() {
        return(l*w);
    }
    public double getPerimeter() {
    	return(2*(l+w));
    }
}
