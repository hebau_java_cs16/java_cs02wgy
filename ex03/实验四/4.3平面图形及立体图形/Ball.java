
public class Ball extends Pillar{//��
	double R;
	Geometry bottom;
	Ball (Geometry bottom,double R){
		this.bottom=bottom;
		this.R=R;
	}
	public double getVolume() {
		return 4*bottom.getArea()*R/3.0;
	}
	public double Surface_area() {
		return 2*bottom.getPerimeter()*R;
	}
}
