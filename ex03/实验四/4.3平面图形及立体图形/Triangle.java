public class Triangle extends Geometry  {//������
	double a;
	double b;
	double c;
	double h;
	Triangle(double a,double b,double c,double h) {
        this.a=a;
        this.b=b;
        this.c=c;
        this.h=h;
    }
	public void seta(double a ) {
		this.a=a;
	}
	public double geta(double a) {
		return a;
	}
	public void setb(double b) {
		this.b=b;
	}
	public double getb() {
		return b;
	}
	public void setc(double c) {
		this.c=c;
	}
	public double getc() {
		return c;
	}
	public void seth(double h) {
		this.h=h;
	}
	public double geth() {
		return h;
	}
    public double getPerimeter() {
        return(a+b+c);
    }
    public double getArea() {
    	return(a*h/2.0);
    }
}
