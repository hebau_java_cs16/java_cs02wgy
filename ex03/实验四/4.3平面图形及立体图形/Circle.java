public class Circle extends Geometry {//Բ
    double r;
    Circle(double r) {
        this.r=r;
    }
    public void setr(double r) {
    	this.r=r;
    }
    public double getr() {
    	return r;
    }
    public double getArea() {
        return(3.14*r*r);
    }
    public double getPerimeter() {
    	return(3.14*2*r);
    }
}