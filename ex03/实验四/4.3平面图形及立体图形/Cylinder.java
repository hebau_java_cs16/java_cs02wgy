
public class Cylinder extends Pillar {//Բ��
	double height;
	Geometry bottom;
	Cylinder (Geometry bottom,double height){
		this.bottom=bottom;
		this.height=height;
	}
	public double getVolume() {
		if(height==0) {
			return -1;
		}
		return bottom.getArea()*height;
	}
	public double Surface_area() {
		return 2*(bottom.getArea())+bottom.getPerimeter()*height;//
	}
}
