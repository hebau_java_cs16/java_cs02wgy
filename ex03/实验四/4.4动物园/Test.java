import java.util.Scanner;
public class Test {
	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("请输入饲养员姓名：");
		String name=input.next();
		Feeder people =new Feeder(name);
    	System.out.println("请输入狮子个数：");
    	int a=input.nextInt();
		Animal []Lion=new Lion[a];
		System.out.println("请输入猴子个数：");
    	int b=input.nextInt();
    	Animal []Monkey=new Monkey[b];
    	System.out.println("请输入鸽子个数：");
    	int c=input.nextInt();
    	Animal []Pigeon=new Pigeon[c];
    	for(int i=0;i<Lion.length;i++) {
    		Lion[i]=new Lion();
    		Lion[i].eat();
    	}
    	for(int i=0;i<Monkey.length;i++) {
    		Monkey[i]=new Monkey();
    		Monkey[i].eat();
    	}
    	for(int i=0;i<Pigeon.length;i++) {
    		Pigeon[i]=new Pigeon();
    		Pigeon[i].eat();
    	}
    	System.out.println("传递数组方式");
    	people.feedAnimal(Lion);
    	people.feedAnimal(Monkey);
    	people.feedAnimal(Pigeon);
	}
}
