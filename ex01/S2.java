import java.util.Scanner;
public class S2 {
	public static void main(String[] args) {
		System.out.println("请输入所要查的年份和月份") ;
		Scanner input=new Scanner(System.in);
        int year=input.nextInt();
        int month=input.nextInt();
      //  int s;
      //  s=days(year,month);
      //  System.out.println(s) ;
      //  s=totalDays(year,month);
      //  System.out.println(s) ;
        printCalender(year,month);
       
	}
	public static boolean isLeap(int year) {//判断闰年
		if(year%400==0||(year%4==0&&year%100!=0)){
			return true ;
		}
		else {
			return false ;
		}
	}
	public static int days(int year,int month) {//判断某年某月有多少天
		int x;
		switch(month) {
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12: x=31;break;
			case 4:
			case 6:
			case 9:
			case 11: x=30;break;
			default :
				if (isLeap(year)) {
					 x=29;
				}
				else {
					 x=28;
				}
			}
		return x;
	}
	public static int totalDays(int year,int month) {//距离1900.1.1的总天数
			int x=0;
			for(int i=1900;i<year;i++) {
				if (isLeap(i)) {
					x+=366;
				}
				else {
					x+=365;
				}
			}
			for(int j=1;j<month;j++){
				int s=days(year,j);
				x+=s;
			}
			return x;
	}
	public static void printCalender(int year,int month) {//输出某月日历
		int x=totalDays(year,month);
		int s=1+x%7;
		int n=0;
		System.out.println("一\t二\t三\t四\t五\t六\t日\t") ;
		for(int i=1;i<s;i++) {
			System.out.print("\t") ;
			n++;
		}
		int j=days(year,month);
		for(int k=1;k<=j;k++) {
			System.out.print(k+"\t") ;
			n++;
			if(n%7==0) {
				System.out.println( ) ;
			}
		}
	}
}